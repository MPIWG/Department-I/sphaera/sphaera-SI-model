#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  5 10:33:42 2022

@author: zamani
"""
import numpy as np

def Dic_Books_Year(df:'DataFram of layer',Books:'Books in the layer'):
    
    """ This function returns a dictionary which keys are the books and
    the values are the year of pyblication  """
    
    Dic_book_year = {}
    for book in Books:
    
        if not df[df['sourceID']==book].empty:

            Dic_book_year[book]=df[df['sourceID']==book].iloc[0]['sourceYear']
        else:
            Dic_book_year[book]=df[df['targetID']==book].iloc[0]['targetYear']
        
    return Dic_book_year

    



def Out_Degree(sortedbooks: 'Sorted list of books',Graph: 'Directed Graph',Dic:'dictionary of book and year'):
    
    """ This is function that calculated the out degree of each book, and returns two lists.
    One is the outdegree and second is the corresponding year of publication for each book"""
    
    
    outdegree = []
    years = []
    for node in sortedbooks:
        if sortedbooks.index(node) != len(sortedbooks)-1:
            outdegree.append(Graph.out_degree(node)/(len(sortedbooks)-1-sortedbooks.index(node)))
            years.append(Dic[node])
            
    return years, outdegree
 


def Dic_Books_Parts(df:'DataFram of layer',Books:'Books in the layer'):
    
    """ This function returns a dictionary which keys are the books and
    the values are a list of the parts in that book"""
    
    Dic_book_parts = {}
    for book in Books:
        
        Dic_book_parts[book] = []
        if not df[df['sourceID']==book].empty:

            Dic_book_parts[book]=np.unique(df[df['sourceID']==book]['edgeParameter'])
        else:
            Dic_book_parts[book]=np.unique(df[df['targetID']==book]['edgeParameter'])
        
    return Dic_book_parts
