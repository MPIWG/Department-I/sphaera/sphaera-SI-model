# Sphaera SI Model
This repository is authored by Maryam Zamani, Max Planck Institute for the History of Science, 2022.


## Sphaera Multiplex Data
The folder 
```
./data_without100
```
contains the csv and json files of the numerous layers of Sphaera Multiplex.
The layers exclude text part 100.


## Code
The code to replicate the layer-wise analysis can be found here 
```
./SI_All_layers_separately.ipynb
```
The code to replicate the component-wise analysis can be found here 
```
./SI_Model.ipynb
```
Helper functions are found in 
```
./Functions.py
```
