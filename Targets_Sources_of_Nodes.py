#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 11:20:31 2019

@author: zamani
"""

import networkx as nx
graph = nx.DiGraph()
NodesList = graph.nodes()
EdgeList = graph.edges()

def Dic_TargetList_of_All_Nodes(NodesList=None, EdgeList=None):
    
    Node_out_dict = {source: [] for source in NodesList}
    for (source, target) in EdgeList:
        Node_out_dict[source].append(target)

    return Node_out_dict



def Dic_SourceList_of_All_Nodes(NodesList=None, EdgeList=None):
    
    Node_In_dict = {target: [] for target in NodesList}
    for (source, target) in EdgeList:
        Node_In_dict[target].append(source)

    return Node_In_dict


def NodesWithZeroOutDegree(graph):
    NodesList = graph.nodes()
    Nodes_No_OutDegree = []
    for node in NodesList:
        if graph.out_degree(node) == 0:
            Nodes_No_OutDegree.append(node)
    return Nodes_No_OutDegree
 

def NodesWithZeroInDEgree(graph):
    NodesList = graph.nodes()       
    Nodes_No_InDegree = []
    for node in NodesList:
        if graph.in_degree(node) == 0:
            Nodes_No_InDegree.append(node)
    return Nodes_No_InDegree